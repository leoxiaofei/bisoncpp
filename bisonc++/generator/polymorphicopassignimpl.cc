#include "generator.ih"

void Generator::polymorphicOpAssignImpl(ostream &out) const
{
    key(out);

    for (auto &poly: d_polymorphic)
        out << 
            "inline SType &SType::operator=(" << poly.second << 
                                                            " const &value)\n"
            "{\n"
            "    assign< Tag_::" << poly.first << " >(value);\n"
            "    return *this;\n"
            "}\n"
            "inline SType &SType::operator=(" << poly.second << " &&tmp)\n"
            "{\n"
            "    assign< Tag_::" << poly.first << " >(std::move(tmp));\n"
            "    return *this;\n"
            "}\n";
}



