#include "generator.ih"

void Generator::tokens(ostream &out) const
{
    Terminal::ConstVector tokens;

    for (auto terminal: d_rules.terminals())
        selectSymbolic(terminal, tokens);

    key(out);
 
    if (!tokens.size())
    {
        out << "// No symbolic tokens were defined\n";
        return;
    }
 
    sort(tokens.begin(), tokens.end(), Terminal::compareValues);

    ofstream outTokens;
    if (not d_options.useTokenPath())
        d_writer.useStream(out);
    else
    {
        outTokens = tokenPath();
        d_writer.useStream(outTokens);
    }

    d_writer.insert(tokens);

    if (d_options.useTokenPath())
    {
        string const &ns = d_options.tokenNameSpace();

                                        // {, matching:
        outTokens << "};\n" <<
                     (
                        ns.empty() ? 
                            "" 
                        : 
                            "\n"        // {, matching:
                            "} // " + ns + '\n'
                     ) <<
                     "\n"
                     "#endif\n";
    }
}



